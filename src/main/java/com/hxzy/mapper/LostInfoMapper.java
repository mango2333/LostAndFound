package com.hxzy.mapper;

import com.hxzy.dto.LostInfoDTO;
import com.hxzy.enetity.LostInfo;

import java.util.List;

/**
 * @Description:
 * @Author: TanBo
 * @Date: 2022/3/8 14:39
 */
public interface LostInfoMapper {
    int insert(LostInfo lostInfo);
    int update(LostInfo lostInfo);
    int delete(String id);
    LostInfo findById(String id);
    List<LostInfo> findByCondition(LostInfoDTO lostInfoDTO);
}
