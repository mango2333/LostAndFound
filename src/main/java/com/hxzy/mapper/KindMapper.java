package com.hxzy.mapper;

import com.hxzy.enetity.KindInfo;

import java.util.List;

/**
 * @Description:
 * @Author: Hao
 * @Date: 2022/3/9 18:40
 */
public interface KindMapper {
    List<KindInfo> findAll();
}
