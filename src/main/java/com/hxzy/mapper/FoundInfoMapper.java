package com.hxzy.mapper;

import com.hxzy.dto.FoundInfoDTO;
import com.hxzy.enetity.FoundInfo;

import java.util.List;

/**
 * @Description:
 * @Author: TanBo
 * @Date: 2022/3/8 14:39
 */
public interface FoundInfoMapper {
    int insert(FoundInfo foundInfo);
    int update(FoundInfo foundInfo);
    int delete(String id);
    FoundInfo findById(String id);
    List<FoundInfo> findByCondition(FoundInfoDTO foundDTO);
}
