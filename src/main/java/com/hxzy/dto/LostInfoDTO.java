package com.hxzy.dto;

import com.alibaba.fastjson.annotation.JSONField;
import com.hxzy.common.dto.BaseDTO;

import javax.validation.constraints.NotBlank;
import java.sql.Timestamp;

/**
 * @Description:
 * @Author: TanBo
 * @Date: 2022/3/8 15:15
 */
public class LostInfoDTO extends BaseDTO {
    @NotBlank(message = "失物ID不能为空")
    private String lostId;
    @NotBlank(message = "失物类型不能为空")
    private String kindId;
    @NotBlank(message = "失物名称不能为空")
    private String lostName;
    private String lostPlace;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Timestamp lostTime;
    private String lostDecp;
    private String lostPhoto;
    private String userId;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Timestamp lostReleaseTime;
    private Integer lostStatus;
    private Integer checkStatus;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Timestamp createTime;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Timestamp updateTime;
    private String foundUserId;

    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Timestamp startTime;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Timestamp endTime;

    public Timestamp getStartTime() {
        return startTime;
    }

    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    public Timestamp getEndTime() {
        return endTime;
    }

    public void setEndTime(Timestamp endTime) {
        this.endTime = endTime;
    }

    public String getLostId() {
        return lostId;
    }

    public void setLostId(String lostId) {
        this.lostId = lostId;
    }

    public String getKindId() {
        return kindId;
    }

    public void setKindId(String kindId) {
        this.kindId = kindId;
    }

    public String getLostName() {
        return lostName;
    }

    public void setLostName(String lostName) {
        this.lostName = lostName;
    }

    public String getLostPlace() {
        return lostPlace;
    }

    public void setLostPlace(String lostPlace) {
        this.lostPlace = lostPlace;
    }

    public Timestamp getLostTime() {
        return lostTime;
    }

    public void setLostTime(Timestamp lostTime) {
        this.lostTime = lostTime;
    }

    public String getLostDecp() {
        return lostDecp;
    }

    public void setLostDecp(String lostDecp) {
        this.lostDecp = lostDecp;
    }

    public String getLostPhoto() {
        return lostPhoto;
    }

    public void setLostPhoto(String lostPhoto) {
        this.lostPhoto = lostPhoto;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Timestamp getLostReleaseTime() {
        return lostReleaseTime;
    }

    public void setLostReleaseTime(Timestamp lostReleaseTime) {
        this.lostReleaseTime = lostReleaseTime;
    }

    public Integer getLostStatus() {
        return lostStatus;
    }

    public void setLostStatus(Integer lostStatus) {
        this.lostStatus = lostStatus;
    }

    public Integer getCheckStatus() {
        return checkStatus;
    }

    public void setCheckStatus(Integer checkStatus) {
        this.checkStatus = checkStatus;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    public String getFoundUserId() {
        return foundUserId;
    }

    public void setFoundUserId(String foundUserId) {
        this.foundUserId = foundUserId;
    }
}
