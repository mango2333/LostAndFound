package com.hxzy.dto;

import javax.validation.constraints.NotBlank;

/**
 * @Description:
 * @Author: Hao
 * @Date: 2022/3/8 14:57
 */
public class LoginUserDTO {
    @NotBlank(message = "用户名不能为空")
    private String loginName;
    @NotBlank(message = "密码不能为空")
    private String loginPwd;
    @NotBlank(message = "验证码不能为空")
    private String code;
    @NotBlank(message = "验证码标识错误")
    private String uuid;

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getLoginPwd() {
        return loginPwd;
    }

    public void setLoginPwd(String loginPwd) {
        this.loginPwd = loginPwd;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
