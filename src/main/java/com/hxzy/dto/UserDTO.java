package com.hxzy.dto;

/**
 * @Description:
 * @Author: Hao
 * @Date: 2022/3/8 18:45
 */
public class UserDTO {
    private String userName;
    private String userPhone;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }
}
