package com.hxzy.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @Description:
 * @Author: TanBo
 * @Date: 2022/2/26 16:00
 */
public class StringUtil {

    /**
     * 判断字符是否为null
     *
     * @param value
     * @return
     */
    public static boolean isEmpty(String value) {
        return null == value;
    }

    /**
     * 判断字符是否不为null
     *
     * @param value
     * @return
     */
    public static boolean isNotEmpty(String value) {
        return null != value;
    }

    /**
     * 判断字符是否为空
     *
     * @param value
     * @return
     */
    public static boolean isBlank(String value) {
        if (isNotEmpty(value)) {
            return value.trim().length() == 0;
        }
        return true;
    }

    /**
     * 判断字符是否不为空
     *
     * @param value
     * @return
     */
    public static boolean isNotBlank(String value) {
        return !isBlank(value);
    }

    /**
     * 判断字符是否为大于0的整数
     *
     * @param value
     * @return
     */
    public static boolean isIntGreaterThanZero(String value) {
        if (isNotBlank(value)) {
            String regStr = "^[1-9]\\\\d*$";
            Pattern compile = Pattern.compile(regStr);
            Matcher matcher = compile.matcher(value);
            return matcher.matches();
        }
        return false;
    }

    /**
     * 判断字符是否为整数
     *
     * @param value
     * @return
     */
    public static boolean isInt(String value) {
        if (isNotBlank(value)) {
            String regStr = "^-?[1-9]\\d*$";
            Pattern compile = Pattern.compile(regStr);
            Matcher matcher = compile.matcher(value);
            return matcher.matches() || "0".equals(value);
        }
        return false;
    }


    /**
     * 判断字符是否为数字(整数，小数)
     *
     * @param value
     * @return
     */
    public static boolean isDigit(String value) {
        if (isNotBlank(value)) {
            //  \\d代表[0-9]   +代表可以出现1次或多次
            String regStr = "^([+-]?)\\d*\\.?\\d+$";
            //1、创建一个正则表达式的解析器
            Pattern pattern = Pattern.compile(regStr);
            //2、创建一个表达式执行匹配器
            Matcher matcher = pattern.matcher(value);
            return matcher.matches();
        }
        return false;
    }
}
