package com.hxzy.util;

import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.InputStream;

/**
 * @Description:
 * @Author: hxzy
 * @Date: 2022/2/24 11:01
 */
public class MybatisUtil {

    //唯一
    private static SqlSessionFactory sqlSessionFactory;

    static{
        InputStream inputStream = MybatisUtil.class.getResourceAsStream("/mybatis.xml");
        sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
    }

    public static SqlSessionFactory getSqlSessionFactory() {
        return sqlSessionFactory;
    }
}
