package com.hxzy.util;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.HashSet;
import java.util.Set;

/**
 * @Description:
 * @Author: whl
 * @Date: 2022/3/1 15:53
 */
public class BeanValidatorUtil {
    public static <T> Set<String> validator(Class<T> clazz,Object obj){

        Set<String> errorSet = new HashSet<>();

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<T>> constraintViolations = validator.validate((T)obj);

        if (constraintViolations.isEmpty()){
            System.out.println("成功");
        }else{
            ConstraintViolation<T> next = constraintViolations.iterator().next();
            errorSet.add(next.getMessage());
        }
        return errorSet;
    }
}
