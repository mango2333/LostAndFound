package com.hxzy.util;

import redis.clients.jedis.Jedis;

/**
 * @Description:
 * @Author: TanBo
 * @Date: 2022/3/3 17:22
 */
public class RedisUtil {

    public static Jedis getRedis(){
        Jedis redis = new Jedis("192.168.27.180",6379);
        redis.auth("hxzy");
        redis.select(1);
        return redis;
    }

}
