package com.hxzy.util;


import com.hxzy.enetity.User;

/**
 *
 * @Description:
 * @Author: whl
 * @Date: 2022/3/4 15:34
 */
public class ThreadLocalUtil {
    //数据存放到 ThreadLocalMap 集合中<ThreadId, Value= Entry>  Entry是一个 WeakReference 弱引用
    //java中数据引用 4种    强，弱，软，虚   jvm处理这几种引时候清除数据方式
    // 强-->就算是OOM,也不会清空内存中数据
    // 弱 -->快要OOM,JVM就会清空弱引用数据
    // 软 -->jvm会不定时候优先清空软引用数据
    // 虚 -->jvm时不时清空数据
    public static ThreadLocal<Object> threadLocal = new ThreadLocal<>();
}
