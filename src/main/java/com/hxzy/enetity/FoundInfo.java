package com.hxzy.enetity;

import com.alibaba.fastjson.annotation.JSONField;

import javax.validation.constraints.NotBlank;
import java.sql.Timestamp;

/**
 * @Description:
 * @Author: TanBo
 * @Date: 2022/3/8 14:01
 */
public class FoundInfo {
    @NotBlank(message = "拾物ID不能为空")
    private String foundId;
    @NotBlank(message = "拾物类型不能为空")
    private String kindId;
    @NotBlank(message = "拾物名称不能为空")
    private String foundName;
    private String foundPlace;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Timestamp foundTime;
    private String foundDecp;
    private String foundPhoto;
    private String userId;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Timestamp foundReleaseTime;
    private Integer foundStatus;
    private Integer checkStatus;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Timestamp createTime;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Timestamp updateTime;

    public FoundInfo() {
    }

    public FoundInfo(String foundId, String kindId, String foundName, String foundPlace, Timestamp foundTime, String foundDecp, String foundPhoto, String userId, Timestamp foundReleaseTime, Integer foundStatus, Integer checkStatus, Timestamp createTime, Timestamp updateTime) {
        this.foundId = foundId;
        this.kindId = kindId;
        this.foundName = foundName;
        this.foundPlace = foundPlace;
        this.foundTime = foundTime;
        this.foundDecp = foundDecp;
        this.foundPhoto = foundPhoto;
        this.userId = userId;
        this.foundReleaseTime = foundReleaseTime;
        this.foundStatus = foundStatus;
        this.checkStatus = checkStatus;
        this.createTime = createTime;
        this.updateTime = updateTime;
    }

    public String getFoundId() {
        return foundId;
    }

    public void setFoundId(String foundId) {
        this.foundId = foundId;
    }

    public String getKindId() {
        return kindId;
    }

    public void setKindId(String kindId) {
        this.kindId = kindId;
    }

    public String getFoundName() {
        return foundName;
    }

    public void setFoundName(String foundName) {
        this.foundName = foundName;
    }

    public String getFoundPlace() {
        return foundPlace;
    }

    public void setFoundPlace(String foundPlace) {
        this.foundPlace = foundPlace;
    }

    public Timestamp getFoundTime() {
        return foundTime;
    }

    public void setFoundTime(Timestamp foundTime) {
        this.foundTime = foundTime;
    }

    public String getFoundDecp() {
        return foundDecp;
    }

    public void setFoundDecp(String foundDecp) {
        this.foundDecp = foundDecp;
    }

    public String getFoundPhoto() {
        return foundPhoto;
    }

    public void setFoundPhoto(String foundPhoto) {
        this.foundPhoto = foundPhoto;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Timestamp getFoundReleaseTime() {
        return foundReleaseTime;
    }

    public void setFoundReleaseTime(Timestamp foundReleaseTime) {
        this.foundReleaseTime = foundReleaseTime;
    }

    public Integer getFoundStatus() {
        return foundStatus;
    }

    public void setFoundStatus(Integer foundStatus) {
        this.foundStatus = foundStatus;
    }

    public Integer getCheckStatus() {
        return checkStatus;
    }

    public void setCheckStatus(Integer checkStatus) {
        this.checkStatus = checkStatus;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "foundInfo{" +
                "foundId='" + foundId + '\'' +
                ", kindId='" + kindId + '\'' +
                ", foundName='" + foundName + '\'' +
                ", foundPlace='" + foundPlace + '\'' +
                ", foundTime=" + foundTime +
                ", foundDecp='" + foundDecp + '\'' +
                ", foundPhoto='" + foundPhoto + '\'' +
                ", userId='" + userId + '\'' +
                ", foundReleaseTime=" + foundReleaseTime +
                ", foundStatus='" + foundStatus + '\'' +
                ", checkStatus='" + checkStatus + '\'' +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }
}
