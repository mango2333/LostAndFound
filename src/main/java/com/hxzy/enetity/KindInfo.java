package com.hxzy.enetity;

import java.sql.Timestamp;

/**
 * @Description:
 * @Author: Hao
 * @Date: 2022/3/9 18:38
 */
public class KindInfo {
    private String kindId;
    private String kindName;
    private Timestamp createTime;
    private Timestamp updateTime;

    public String getKindId() {
        return kindId;
    }

    public void setKindId(String kindId) {
        this.kindId = kindId;
    }

    public String getKindName() {
        return kindName;
    }

    public void setKindName(String kindName) {
        this.kindName = kindName;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }
}
