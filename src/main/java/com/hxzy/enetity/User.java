package com.hxzy.enetity;

import com.alibaba.fastjson.annotation.JSONField;

import javax.validation.constraints.NotBlank;
import java.sql.Timestamp;

/**
 * @Description:
 * @Author: TanBo
 * @Date: 2022/3/8 13:56
 */
public class User {
    private String userId;
    private String userNick;
    @NotBlank(message = "用户名不能为空")
    private String userName;
    private String stuNum;
    @NotBlank(message = "密码不能为空")
    private String userPwd;
    @NotBlank(message = "手机号不能为空")
    private String userPhone;
    private String wechatId;
    private String numId;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Timestamp createTime;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Timestamp updateTime;

    public User() {
    }

    public User(String userNick, String userName, String stuNum, String userPwd, String userPhone, String wechatId, String numId, Timestamp createTime, Timestamp updateTime) {
        this.userNick = userNick;
        this.userName = userName;
        this.stuNum = stuNum;
        this.userPwd = userPwd;
        this.userPhone = userPhone;
        this.wechatId = wechatId;
        this.numId = numId;
        this.createTime = createTime;
        this.updateTime = updateTime;
    }

    public User(String userId, String userNick, String userName, String stuNum, String userPwd, String userPhone, String wechatId, String numId, Timestamp createTime, Timestamp updateTime) {
        this.userId = userId;
        this.userNick = userNick;
        this.userName = userName;
        this.stuNum = stuNum;
        this.userPwd = userPwd;
        this.userPhone = userPhone;
        this.wechatId = wechatId;
        this.numId = numId;
        this.createTime = createTime;
        this.updateTime = updateTime;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserNick() {
        return userNick;
    }

    public void setUserNick(String userNick) {
        this.userNick = userNick;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getStuNum() {
        return stuNum;
    }

    public void setStuNum(String stuNum) {
        this.stuNum = stuNum;
    }

    public String getUserPwd() {
        return userPwd;
    }

    public void setUserPwd(String userPwd) {
        this.userPwd = userPwd;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getWechatId() {
        return wechatId;
    }

    public void setWechatId(String wechatId) {
        this.wechatId = wechatId;
    }

    public String getNumId() {
        return numId;
    }

    public void setNumId(String numId) {
        this.numId = numId;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "User{" +
                "userId='" + userId + '\'' +
                ", userNick='" + userNick + '\'' +
                ", userName='" + userName + '\'' +
                ", stuNum='" + stuNum + '\'' +
                ", userPwd='" + userPwd + '\'' +
                ", userPhone='" + userPhone + '\'' +
                ", wechatId='" + wechatId + '\'' +
                ", numId='" + numId + '\'' +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }
}
