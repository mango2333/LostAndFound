package com.hxzy.enetity;

import com.alibaba.fastjson.annotation.JSONField;

import javax.validation.constraints.NotBlank;
import java.sql.Timestamp;

/**
 * @Description:
 * @Author: TanBo
 * @Date: 2022/3/8 14:01
 */
public class LostInfo {
    @NotBlank(message = "失物ID不能为空")
    private String lostId;
    @NotBlank(message = "失物类型不能为空")
    private String kindId;
    @NotBlank(message = "失物名称不能为空")
    private String lostName;
    private String lostPlace;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Timestamp lostTime;
    private String lostDecp;
    private String lostPhoto;
    private String userId;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Timestamp lostReleaseTime;
    private Integer lostStatus;
    private Integer checkStatus;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Timestamp createTime;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Timestamp updateTime;
    private String foundUserId;

    public LostInfo() {
    }

    public LostInfo(String lostId, String kindId, String lostName, String lostPlace, Timestamp lostTime, String lostDecp, String lostPhoto, String userId, Timestamp lostReleaseTime, Integer lostStatus, Integer checkStatus, Timestamp createTime, Timestamp updateTime, String foundUserId) {
        this.lostId = lostId;
        this.kindId = kindId;
        this.lostName = lostName;
        this.lostPlace = lostPlace;
        this.lostTime = lostTime;
        this.lostDecp = lostDecp;
        this.lostPhoto = lostPhoto;
        this.userId = userId;
        this.lostReleaseTime = lostReleaseTime;
        this.lostStatus = lostStatus;
        this.checkStatus = checkStatus;
        this.createTime = createTime;
        this.updateTime = updateTime;
        this.foundUserId = foundUserId;
    }

    public String getLostId() {
        return lostId;
    }

    public void setLostId(String lostId) {
        this.lostId = lostId;
    }

    public String getKindId() {
        return kindId;
    }

    public void setKindId(String kindId) {
        this.kindId = kindId;
    }

    public String getLostName() {
        return lostName;
    }

    public void setLostName(String lostName) {
        this.lostName = lostName;
    }

    public String getLostPlace() {
        return lostPlace;
    }

    public void setLostPlace(String lostPlace) {
        this.lostPlace = lostPlace;
    }

    public Timestamp getLostTime() {
        return lostTime;
    }

    public void setLostTime(Timestamp lostTime) {
        this.lostTime = lostTime;
    }

    public String getLostDecp() {
        return lostDecp;
    }

    public void setLostDecp(String lostDecp) {
        this.lostDecp = lostDecp;
    }

    public String getLostPhoto() {
        return lostPhoto;
    }

    public void setLostPhoto(String lostPhoto) {
        this.lostPhoto = lostPhoto;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Timestamp getLostReleaseTime() {
        return lostReleaseTime;
    }

    public void setLostReleaseTime(Timestamp lostReleaseTime) {
        this.lostReleaseTime = lostReleaseTime;
    }

    public Integer getLostStatus() {
        return lostStatus;
    }

    public void setLostStatus(Integer lostStatus) {
        this.lostStatus = lostStatus;
    }

    public Integer getCheckStatus() {
        return checkStatus;
    }

    public void setCheckStatus(Integer checkStatus) {
        this.checkStatus = checkStatus;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    public String getFoundUserId() {
        return foundUserId;
    }

    public void setFoundUserId(String foundUserId) {
        this.foundUserId = foundUserId;
    }

    @Override
    public String toString() {
        return "LostInfo{" +
                "lostId='" + lostId + '\'' +
                ", kindId='" + kindId + '\'' +
                ", lostName='" + lostName + '\'' +
                ", lostPlace='" + lostPlace + '\'' +
                ", lostTime=" + lostTime +
                ", lostDecp='" + lostDecp + '\'' +
                ", lostPhoto='" + lostPhoto + '\'' +
                ", userId='" + userId + '\'' +
                ", lostReleaseTime=" + lostReleaseTime +
                ", lostStatus='" + lostStatus + '\'' +
                ", checkStatus='" + checkStatus + '\'' +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                ", foundUserId='" + foundUserId + '\'' +
                '}';
    }
}
