package com.hxzy.service.impl;

import com.hxzy.enetity.KindInfo;
import com.hxzy.mapper.KindMapper;
import com.hxzy.service.KindService;
import com.hxzy.util.MybatisUtil;
import org.apache.ibatis.session.SqlSession;

import java.util.List;

/**
 * @Description:
 * @Author: Hao
 * @Date: 2022/3/9 18:43
 */
public class KindServiceImpl implements KindService {
    @Override
    public List<KindInfo> findAll() {
        List<KindInfo> kindInfoList;
        try (
                SqlSession sqlSession = MybatisUtil.getSqlSessionFactory().openSession()
        ) {
            KindMapper mapper = sqlSession.getMapper(KindMapper.class);
            kindInfoList = mapper.findAll();

        }
        return kindInfoList;
    }
}
