package com.hxzy.service.impl;

import com.hxzy.dto.UserDTO;
import com.hxzy.enetity.User;
import com.hxzy.mapper.UserMapper;
import com.hxzy.service.UserService;
import com.hxzy.util.MybatisUtil;
import org.apache.ibatis.session.SqlSession;

import java.util.List;

/**
 * @Description:
 * @Author: Hao
 * @Date: 2022/3/8 14:49
 */
public class UserServiceImpl implements UserService {
    @Override
    public int insert(User user) {
        int count =0;
        try (
                SqlSession sqlSession = MybatisUtil.getSqlSessionFactory().openSession()
        ) {
            UserMapper mapper = sqlSession.getMapper(UserMapper.class);
            count = mapper.insert(user);
            sqlSession.commit();
        }
        return count;
    }

    @Override
    public int update(User user) {
        int count =0;
        try (
                SqlSession sqlSession = MybatisUtil.getSqlSessionFactory().openSession()
        ) {
            UserMapper mapper = sqlSession.getMapper(UserMapper.class);
            count = mapper.update(user);
            sqlSession.commit();
        }
        return count;
    }

    @Override
    public int delete(String id) {
        int count =0;
        try (
                SqlSession sqlSession = MybatisUtil.getSqlSessionFactory().openSession()
        ) {
            UserMapper mapper = sqlSession.getMapper(UserMapper.class);
            count = mapper.delete(id);
            sqlSession.commit();
        }
        return count;
    }

    @Override
    public int findById(String id) {
        return 0;
    }

    @Override
    public User searchByName(String name) {
        User user = null;
        try (
                SqlSession sqlSession = MybatisUtil.getSqlSessionFactory().openSession()
        ) {
            UserMapper mapper = sqlSession.getMapper(UserMapper.class);
            user = mapper.searchByName(name);
        }
        return user;
    }

    @Override
    public List<User> search(UserDTO userDTO) {
        List<User> search = null;
        try (
                SqlSession sqlSession = MybatisUtil.getSqlSessionFactory().openSession()
        ) {
            UserMapper mapper = sqlSession.getMapper(UserMapper.class);
            search = mapper.search(userDTO);
        }
        return search;
    }

    @Override
    public int extitsName(User user) {
        int count = 0;
        try (
                SqlSession sqlSession = MybatisUtil.getSqlSessionFactory().openSession()
        ) {
            UserMapper mapper = sqlSession.getMapper(UserMapper.class);
            count = mapper.extitsName(user);
        }
        return count;
    }
}
