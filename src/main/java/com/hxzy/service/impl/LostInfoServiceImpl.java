package com.hxzy.service.impl;

import com.hxzy.dto.LostInfoDTO;
import com.hxzy.enetity.LostInfo;
import com.hxzy.mapper.LostInfoMapper;
import com.hxzy.service.LostInfoService;
import com.hxzy.util.MybatisUtil;
import org.apache.ibatis.session.SqlSession;

import java.util.List;

/**
 * @Description:
 * @Author: TanBo
 * @Date: 2022/3/8 15:28
 */
public class LostInfoServiceImpl implements LostInfoService {

    @Override
    public int insert(LostInfo lostInfo) {
        int count = 0;
        try (SqlSession session = MybatisUtil.getSqlSessionFactory().openSession()) {
            LostInfoMapper mapper = session.getMapper(LostInfoMapper.class);
            count = mapper.insert(lostInfo);
            session.commit();
        }
        return count;
    }

    @Override
    public int update(LostInfo lostInfo) {
        int count = 0;
        try (SqlSession session = MybatisUtil.getSqlSessionFactory().openSession()) {
            LostInfoMapper mapper = session.getMapper(LostInfoMapper.class);
            count = mapper.update(lostInfo);
            session.commit();
        }
        return count;
    }

    @Override
    public int delete(String id) {
        int count = 0;
        try (SqlSession session = MybatisUtil.getSqlSessionFactory().openSession()) {
            LostInfoMapper mapper = session.getMapper(LostInfoMapper.class);
            count = mapper.delete(id);
            session.commit();
        }
        return count;
    }

    @Override
    public int delete(List<String> ids) {
        int count = 0;
        try (SqlSession session = MybatisUtil.getSqlSessionFactory().openSession()) {
            LostInfoMapper mapper = session.getMapper(LostInfoMapper.class);
            for (int i = 0; i < ids.size(); i++) {
                count += mapper.delete(ids.get(i));
            }
            if (count == ids.size()){
                session.commit();
                return 1;
            }else {
                session.rollback();
                return 0;
            }
        }
    }

    @Override
    public LostInfo findById(String id) {
        LostInfo lostInfo;
        try (SqlSession session = MybatisUtil.getSqlSessionFactory().openSession()) {
            LostInfoMapper mapper = session.getMapper(LostInfoMapper.class);
            lostInfo = mapper.findById(id);
            session.commit();
        }
        return lostInfo;
    }

    @Override
    public List<LostInfo> findByCondition(LostInfoDTO lostInfoDTO) {
        List<LostInfo> list;
        try (SqlSession session = MybatisUtil.getSqlSessionFactory().openSession()) {
            LostInfoMapper mapper = session.getMapper(LostInfoMapper.class);
            list = mapper.findByCondition(lostInfoDTO);
            session.commit();
        }
        return list;
    }
}
