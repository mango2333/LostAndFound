package com.hxzy.service.impl;

import com.hxzy.dto.FoundInfoDTO;
import com.hxzy.enetity.FoundInfo;
import com.hxzy.mapper.FoundInfoMapper;
import com.hxzy.service.FoundInfoService;
import com.hxzy.util.MybatisUtil;
import org.apache.ibatis.session.SqlSession;

import java.util.List;

/**
 * @Description:
 * @Author: TanBo
 * @Date: 2022/3/8 15:28
 */
public class FoundInfoServiceImpl implements FoundInfoService {

    @Override
    public int insert(FoundInfo foundInfo) {
        int count = 0;
        try (SqlSession session = MybatisUtil.getSqlSessionFactory().openSession()) {
            FoundInfoMapper mapper = session.getMapper(FoundInfoMapper.class);
            count = mapper.insert(foundInfo);
            session.commit();
        }
        return count;
    }

    @Override
    public int update(FoundInfo foundInfo) {
        int count = 0;
        try (SqlSession session = MybatisUtil.getSqlSessionFactory().openSession()) {
            FoundInfoMapper mapper = session.getMapper(FoundInfoMapper.class);
            count = mapper.update(foundInfo);
            session.commit();
        }
        return count;
    }

    @Override
    public int delete(String id) {
        int count = 0;
        try (SqlSession session = MybatisUtil.getSqlSessionFactory().openSession()) {
            FoundInfoMapper mapper = session.getMapper(FoundInfoMapper.class);
            count = mapper.delete(id);
            session.commit();
        }
        return count;
    }

    @Override
    public int delete(List<String> ids) {
        int count = 0;
        try (SqlSession session = MybatisUtil.getSqlSessionFactory().openSession()) {
            FoundInfoMapper mapper = session.getMapper(FoundInfoMapper.class);
            for (int i = 0; i < ids.size(); i++) {
                count += mapper.delete(ids.get(i));
            }
            if (count == ids.size()){
                session.commit();
                return 1;
            }else {
                session.rollback();
                return 0;
            }
        }
    }

    @Override
    public FoundInfo findById(String id) {
        FoundInfo foundInfo;
        try (SqlSession session = MybatisUtil.getSqlSessionFactory().openSession()) {
            FoundInfoMapper mapper = session.getMapper(FoundInfoMapper.class);
            foundInfo = mapper.findById(id);
            session.commit();
        }
        return foundInfo;
    }

    @Override
    public List<FoundInfo> findByCondition(FoundInfoDTO foundDTO) {
        List<FoundInfo> list;
        try (SqlSession session = MybatisUtil.getSqlSessionFactory().openSession()) {
            FoundInfoMapper mapper = session.getMapper(FoundInfoMapper.class);
            list = mapper.findByCondition(foundDTO);
            session.commit();
        }
        return list;
    }
}
