package com.hxzy.service;

import com.hxzy.dto.UserDTO;
import com.hxzy.enetity.User;

import java.util.List;

/**
 * @Description:
 * @Author: TanBo
 * @Date: 2022/3/8 14:40
 */
public interface UserService {
    int insert(User user);
    int update(User user);
    int delete(String id);
    int findById(String id);
    User searchByName(String name);
    List<User> search(UserDTO userDTO);
    int extitsName(User user);
}
