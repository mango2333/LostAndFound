package com.hxzy.service;

import com.hxzy.enetity.KindInfo;

import java.util.List;

/**
 * @Description:
 * @Author: Hao
 * @Date: 2022/3/9 18:42
 */
public interface KindService {
    List<KindInfo> findAll();
}
