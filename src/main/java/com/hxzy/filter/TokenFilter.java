package com.hxzy.filter;

import com.alibaba.fastjson.JSONObject;
import com.hxzy.common.ResultAjax;
import com.hxzy.util.RedisUtil;
import com.hxzy.util.StringUtil;
import com.hxzy.util.ThreadLocalUtil;
import redis.clients.jedis.Jedis;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

/**
 * @Description:
 * @Author: Hao
 * @Date: 2022/3/8 15:00
 */
@WebFilter(urlPatterns = "/*",initParams = {
        @WebInitParam(name = "exu",value = ".js,.css,.jpg,.png,.gif,.nico,/login,/captcha")
})
public class TokenFilter implements Filter {
    List<String> exuArray = new ArrayList<>();
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        String exu = filterConfig.getInitParameter("exu");
        exuArray = Arrays.asList(exu.split(","));
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletResponse resp = (HttpServletResponse) servletResponse;
        HttpServletRequest rsq = (HttpServletRequest) servletRequest;

        String uri = rsq.getRequestURI();
        long count = exuArray.stream().filter(a -> uri.lastIndexOf(a) >= 0).count();
        if (count > 0){
            filterChain.doFilter(rsq,resp);
        }else {
            String token = rsq.getHeader("token");
            if ( token == null ){
                ResultAjax error = ResultAjax.error(101, "未登录，请重新登录");
                render(resp,error);
                return;
            }

            Jedis redis = RedisUtil.getRedis();
            String key = "token:" +token;
            String s = redis.get(key);
            if (s == null){
                ResultAjax error = ResultAjax.error(301, "令牌已失效");
                render(resp,error);
                return;
            }


            JSONObject object = JSONObject.parseObject(s);
            ThreadLocalUtil.threadLocal.set(object);
            filterChain.doFilter(rsq,resp);
        }

    }

    @Override
    public void destroy() {
        this.exuArray.clear();
    }

    protected void render(HttpServletResponse resp, ResultAjax json) throws IOException {

        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("application/json;charset=UTF-8");

        String text = JSONObject.toJSONString(json);

        PrintWriter out = resp.getWriter();

        out.write(text);
        out.flush();
        out.close();
    }

}
