package com.hxzy.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Description:
 * @Author: TanBo
 * @Date: 2022/2/28 17:08
 */
@WebFilter("/*")
public class CorsFilter implements Filter {
    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain filterChain) throws IOException, ServletException {
        HttpServletResponse response = (HttpServletResponse) resp;
        //允许放行的域名有哪些
        response.setHeader("Access-Control-Allow-Origin", "*");
        //是否允许客户推荐cookie参数
        response.setHeader("Access-Control-Allow-Credentials", "true");
        //允许放行的访问方式 GET/POST/PUT/DELETE/OPTIONS.....
        response.setHeader("Access-Control-Allow-Methods", "*");
        //客户访问缓存时间 1小时
        response.setHeader("Access-Control-Max-Age", "3600");
        //自定义头
        response.setHeader("Access-Control-Allow-Headers", "*");
        response.setHeader("Access-Control-Expose-Headers", "*");

        //放行给下一个过滤器Filter
        filterChain.doFilter(req, response);
    }
}
