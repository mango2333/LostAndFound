package com.hxzy.controller;

import com.alibaba.fastjson.JSONObject;
import com.hxzy.common.BaseServlet;
import com.hxzy.common.ResultAjax;
import com.hxzy.dto.LoginUserDTO;
import com.hxzy.enetity.User;
import com.hxzy.service.impl.UserServiceImpl;
import com.hxzy.util.BeanValidatorUtil;
import com.hxzy.util.RedisUtil;
import org.mindrot.jbcrypt.BCrypt;
import redis.clients.jedis.Jedis;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Set;

/**
 * @Description:
 * @Author: Hao
 * @Date: 2022/3/8 15:35
 */
@WebServlet(urlPatterns = "/api/login")
public class Login extends BaseServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LoginUserDTO loginUserDTO = getInstanceByJsonParameters(req, LoginUserDTO.class);

        Set<String> validator = BeanValidatorUtil.validator(LoginUserDTO.class, loginUserDTO);
        if (!validator.isEmpty()){
            ResultAjax error = ResultAjax.error(100,validator.iterator().next());
            render(resp, JSONObject.toJSONString(error));
            return;
        }

        Jedis redis = RedisUtil.getRedis();
        String key = "captchaKey:" + loginUserDTO.getUuid();
        String s = redis.get(key);

        if (s != null) {
            redis.del(key);
        }
        if(s == null){
            ResultAjax error = ResultAjax.error(301, "图片验证码失效");
            render(resp,JSONObject.toJSONString(error));
            return;
        }
        if (!s.equalsIgnoreCase(loginUserDTO.getCode())){
            ResultAjax error = ResultAjax.error(302, "图片验证码错误");
            render(resp,JSONObject.toJSONString(error));
            return;
        }

        UserServiceImpl userService = new UserServiceImpl();
        User user = userService.searchByName(loginUserDTO.getLoginName());
        if (user == null){
            ResultAjax error = ResultAjax.error(401, "用户名或密码错误");
            render(resp,JSONObject.toJSONString(error));
            return;
        }

        if (!user.getUserPwd().equals(loginUserDTO.getLoginPwd())){
            ResultAjax error = ResultAjax.error(401, "用户名或密码错误");
            render(resp,JSONObject.toJSONString(error));
            return;
        }

        String id = req.getSession().getId();
        String RedisKey = "token:" + id;

        redis.setex(RedisKey,60*60,JSONObject.toJSONString(user));
        HashMap<String, Object> map = new HashMap<>();
        map.put("token",id);
        ResultAjax success = ResultAjax.success(map);
        render(resp,JSONObject.toJSONString(success));


    }
}
