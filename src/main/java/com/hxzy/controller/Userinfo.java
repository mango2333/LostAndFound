package com.hxzy.controller;

import com.hxzy.common.BaseServlet;
import com.hxzy.common.ResultAjax;
import com.hxzy.util.ThreadLocalUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Description:
 * @Author: Hao
 * @Date: 2022/3/8 16:40
 */
@WebServlet(urlPatterns = "/api/userinfo")
public class Userinfo extends BaseServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Object user = ThreadLocalUtil.threadLocal.get();
        ResultAjax success = ResultAjax.success(user);
        render(resp,success);
    }
}
