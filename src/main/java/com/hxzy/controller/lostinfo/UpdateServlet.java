package com.hxzy.controller.lostinfo;

import com.hxzy.common.BaseServlet;
import com.hxzy.common.ResultAjax;
import com.hxzy.enetity.LostInfo;
import com.hxzy.service.LostInfoService;
import com.hxzy.service.impl.LostInfoServiceImpl;
import com.hxzy.util.BeanValidatorUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.util.Set;

/**
 * @Description:
 * @Author: TanBo
 * @Date: 2022/3/8 16:47
 */
@WebServlet("/api/lostInfo/update")
public class UpdateServlet extends BaseServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            LostInfo lostInfo = super.getInstanceByRequest(req, LostInfo.class);
            LostInfoService service = new LostInfoServiceImpl();
            int update = service.update(lostInfo);
            super.render(resp, update);
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }


    }
}
