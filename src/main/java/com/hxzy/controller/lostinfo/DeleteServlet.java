package com.hxzy.controller.lostinfo;

import com.alibaba.fastjson.JSONObject;
import com.hxzy.common.BaseServlet;
import com.hxzy.enetity.LostInfo;
import com.hxzy.service.LostInfoService;
import com.hxzy.service.impl.LostInfoServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @Description:
 * @Author: TanBo
 * @Date: 2022/3/8 16:16
 */
@WebServlet("/api/lostInfo/delete")
public class DeleteServlet extends BaseServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<String> ids = new ArrayList<>();
        List<LostInfo> list = super.getArrayByJsonParameters(req, LostInfo.class);
        for (LostInfo lostInfo : list) {
            ids.add(lostInfo.getLostId());
        }

        LostInfoService service = new LostInfoServiceImpl();
        int delete = service.delete(ids);
        super.render(resp, delete);

    }
}
