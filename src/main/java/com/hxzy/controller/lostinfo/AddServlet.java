package com.hxzy.controller.lostinfo;

import com.hxzy.common.BaseServlet;
import com.hxzy.common.ResultAjax;
import com.hxzy.enetity.LostInfo;
import com.hxzy.service.LostInfoService;
import com.hxzy.service.impl.LostInfoServiceImpl;
import com.hxzy.util.BeanValidatorUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.util.Set;
import java.util.UUID;

/**
 * @Description:
 * @Author: TanBo
 * @Date: 2022/3/8 15:25
 */
@WebServlet("/api/lostInfo/add")
public class AddServlet extends BaseServlet {
    //kindId=食物&lostName=苹果&lostPlace=厕所&lostTime=2077-7-17 07:07:07&lostDecp=红色，苹果，懂？&userId=1
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            //读取参数  得到实例
            LostInfo lostInfo = super.getInstanceByRequest(req, LostInfo.class);
            //设置随机的失物ID
            lostInfo.setLostId(UUID.randomUUID().toString());
            //验证实例是否有效
            Set<String> validator = BeanValidatorUtil.validator(LostInfo.class, lostInfo);
            if (validator.isEmpty()){
                //错误为空则成功 进行添加操作
                LostInfoService service = new LostInfoServiceImpl();
                int insert = service.insert(lostInfo);
                super.render(resp,insert);
            }else {
                //返回错误信息
                String message = validator.iterator().next();
                super.render(resp, ResultAjax.error(message));
            }

        } catch (ParseException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

    }
}
