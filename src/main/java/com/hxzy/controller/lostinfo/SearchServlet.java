package com.hxzy.controller.lostinfo;

import com.github.pagehelper.PageInfo;
import com.hxzy.common.BaseServlet;
import com.hxzy.common.ResultAjax;
import com.hxzy.dto.LostInfoDTO;
import com.hxzy.enetity.LostInfo;
import com.hxzy.service.LostInfoService;
import com.hxzy.service.impl.LostInfoServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;

/**
 * @Description:
 * @Author: TanBo
 * @Date: 2022/3/8 17:08
 */
@WebServlet("/api/lostInfo/search")
public class SearchServlet extends BaseServlet {
    @Override
        protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.startPage(req);
        try {
            LostInfoDTO lostInfoDTO = super.getInstanceByRequest(req, LostInfoDTO.class);
            LostInfoService service = new LostInfoServiceImpl();
            List<LostInfo> list = service.findByCondition(lostInfoDTO);
            PageInfo<LostInfo> pageInfo = new PageInfo<>(list);
            super.render(resp, ResultAjax.success(pageInfo));

        } catch (ParseException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
