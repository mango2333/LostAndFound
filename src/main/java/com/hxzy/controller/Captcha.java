package com.hxzy.controller;

import com.alibaba.fastjson.JSONObject;
import com.hxzy.common.BaseServlet;
import com.hxzy.common.ResultAjax;
import com.hxzy.util.RedisUtil;
import com.wf.captcha.ArithmeticCaptcha;
import redis.clients.jedis.Jedis;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;

/**
 * @Description:
 * @Author: Hao
 * @Date: 2022/3/8 15:11
 */
@WebServlet(urlPatterns = "/api/captcha")
public class Captcha extends BaseServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int with = 150;
        int height = 50;

        ArithmeticCaptcha captcha = new ArithmeticCaptcha(with, height);
        captcha.setLen(2);
        String text = captcha.text();
        String image = captcha.toBase64();

        String id = req.getSession().getId();
        String key = "captchaKey:" + id;

        Jedis redis = RedisUtil.getRedis();
        redis.setex(key,5*60,text);


        ResultAjax success = ResultAjax.success("成功",image);
        success.put("uuid",id);
        render(resp, JSONObject.toJSONString(success));
    }
}
