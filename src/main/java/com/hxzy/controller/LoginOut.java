package com.hxzy.controller;

import com.hxzy.common.BaseServlet;
import com.hxzy.common.ResultAjax;
import com.hxzy.util.RedisUtil;
import redis.clients.jedis.Jedis;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Description:
 * @Author: Hao
 * @Date: 2022/3/8 16:43
 */
@WebServlet(urlPatterns = "/api/loginout")
public class LoginOut extends BaseServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String token = req.getHeader("token");
        Jedis redis = RedisUtil.getRedis();
        String key = "token:" + token;
        redis.del(key);
        ResultAjax success = ResultAjax.success();
        render(resp,success);
    }
}
