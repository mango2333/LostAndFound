package com.hxzy.controller.KindInfo;

import com.hxzy.common.BaseServlet;
import com.hxzy.common.ResultAjax;
import com.hxzy.enetity.KindInfo;
import com.hxzy.service.impl.KindServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @Description:
 * @Author: Hao
 * @Date: 2022/3/9 18:52
 */
@WebServlet(urlPatterns = "/api/kind/all")
public class KindAllServlet extends BaseServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        KindServiceImpl kindService = new KindServiceImpl();
        List<KindInfo> kindInfoList = kindService.findAll();
        if (kindInfoList == null){
            render(resp,ResultAjax.error());
            return;
        }
        render(resp,ResultAjax.success(kindInfoList));
    }
}
