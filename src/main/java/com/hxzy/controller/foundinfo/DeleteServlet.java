package com.hxzy.controller.foundinfo;

import com.hxzy.common.BaseServlet;
import com.hxzy.enetity.FoundInfo;
import com.hxzy.service.FoundInfoService;
import com.hxzy.service.impl.FoundInfoServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @Description:
 * @Author: TanBo
 * @Date: 2022/3/8 16:16
 */
@WebServlet("/api/foundInfo/delete")
public class DeleteServlet extends BaseServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<String> ids = new ArrayList<>();
        List<FoundInfo> list = super.getArrayByJsonParameters(req, FoundInfo.class);
        for (FoundInfo foundInfo : list) {
            ids.add(foundInfo.getFoundId());
        }

        FoundInfoService service = new FoundInfoServiceImpl();
        int delete = service.delete(ids);
        super.render(resp, delete);
    }
}
