package com.hxzy.controller.foundinfo;

import com.github.pagehelper.PageInfo;
import com.hxzy.common.BaseServlet;
import com.hxzy.common.ResultAjax;
import com.hxzy.dto.FoundInfoDTO;
import com.hxzy.enetity.FoundInfo;
import com.hxzy.service.FoundInfoService;
import com.hxzy.service.impl.FoundInfoServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;

/**
 * @Description:
 * @Author: TanBo
 * @Date: 2022/3/8 17:08
 */
@WebServlet("/api/foundInfo/search")
public class SearchServlet extends BaseServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.startPage(req);
        try {
            FoundInfoDTO foundInfoDTO = super.getInstanceByRequest(req, FoundInfoDTO.class);
            FoundInfoService service = new FoundInfoServiceImpl();
            List<FoundInfo> list = service.findByCondition(foundInfoDTO);
            PageInfo<FoundInfo> pageInfo = new PageInfo<>(list);
            super.render(resp, ResultAjax.success(pageInfo));

        } catch (ParseException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
