package com.hxzy.controller.foundinfo;

import com.hxzy.common.BaseServlet;
import com.hxzy.common.ResultAjax;
import com.hxzy.enetity.FoundInfo;
import com.hxzy.service.FoundInfoService;
import com.hxzy.service.impl.FoundInfoServiceImpl;
import com.hxzy.util.BeanValidatorUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.util.Set;

/**
 * @Description:
 * @Author: TanBo
 * @Date: 2022/3/8 16:47
 */
@WebServlet("/api/foundInfo/update")
public class UpdateServlet extends BaseServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            FoundInfo foundInfoDTO = super.getInstanceByRequest(req, FoundInfo.class);
            FoundInfoService service = new FoundInfoServiceImpl();
            int update = service.update(foundInfoDTO);
            super.render(resp, update);
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
