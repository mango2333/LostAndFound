package com.hxzy.controller.user;

import com.hxzy.common.BaseServlet;
import com.hxzy.common.ResultAjax;
import com.hxzy.service.impl.UserServiceImpl;
import com.hxzy.util.StringUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Description:
 * @Author: Hao
 * @Date: 2022/3/9 20:26
 */
@WebServlet(urlPatterns = "/api/user/delete")
public class DeleteServlet extends BaseServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("userId");
        if(StringUtil.isNotBlank(id)){
            UserServiceImpl userService = new UserServiceImpl();
            int delete = userService.delete(id);
            if(delete <= 0){
                render(resp, ResultAjax.error());
                return;
            }
            render(resp,ResultAjax.success());
        }
    }
}
