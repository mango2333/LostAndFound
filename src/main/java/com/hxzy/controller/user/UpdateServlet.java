package com.hxzy.controller.user;

import com.github.pagehelper.PageInfo;
import com.hxzy.common.BaseServlet;
import com.hxzy.common.ResultAjax;
import com.hxzy.dto.UserDTO;
import com.hxzy.enetity.User;
import com.hxzy.service.impl.UserServiceImpl;
import com.hxzy.util.BeanValidatorUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Set;

/**
 * @Description:
 * @Author: Hao
 * @Date: 2022/3/9 19:16
 */
@WebServlet(urlPatterns = "/api/user/update")
public class UpdateServlet extends BaseServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User user = getInstanceByJsonParameters(req, User.class);
        Set<String> validator = BeanValidatorUtil.validator(User.class, user);
        if (!validator.isEmpty()){
            render(resp, ResultAjax.error(validator.iterator().next()));
            return;
        }
        UserServiceImpl userService = new UserServiceImpl();
        int update = userService.update(user);
        if (update <= 0){
            render(resp, ResultAjax.error("更新失败"));
            return;
        }
        render(resp, ResultAjax.success());
    }
}
