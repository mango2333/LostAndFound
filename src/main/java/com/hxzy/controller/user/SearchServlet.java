package com.hxzy.controller.user;

import com.github.pagehelper.PageInfo;
import com.hxzy.common.BaseServlet;
import com.hxzy.common.ResultAjax;
import com.hxzy.dto.UserDTO;
import com.hxzy.enetity.User;
import com.hxzy.service.impl.UserServiceImpl;
import com.hxzy.util.BeanValidatorUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import java.util.Set;

/**
 * @Description:
 * @Author: Hao
 * @Date: 2022/3/8 19:18
 */
@WebServlet(urlPatterns = "/api/user/search")
public class SearchServlet extends BaseServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        startPage(req);
        try {
            UserDTO userDTO = getInstanceByParameters(req, UserDTO.class);
            Set<String> validator = BeanValidatorUtil.validator(User.class, userDTO);
            if (!validator.isEmpty()){
                render(resp,ResultAjax.error(validator.iterator().next()));
                return;
            }
            UserServiceImpl userService = new UserServiceImpl();
            List<User> search = userService.search(userDTO);
            PageInfo<User> userPageInfo = new PageInfo<>(search);
            render(resp, ResultAjax.success(userPageInfo));
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }
}
