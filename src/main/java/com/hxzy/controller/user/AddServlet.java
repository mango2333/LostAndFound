package com.hxzy.controller.user;

import com.hxzy.common.BaseServlet;
import com.hxzy.common.ResultAjax;
import com.hxzy.enetity.User;
import com.hxzy.service.impl.UserServiceImpl;
import com.hxzy.util.BeanValidatorUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Set;
import java.util.UUID;

/**
 * @Description:
 * @Author: TanBo
 * @Date: 2022/3/8 14:50
 */

@WebServlet(urlPatterns = "/api/user/add")
public class AddServlet extends BaseServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //读取参数  得到实例
        User user = super.getInstanceByJsonParameters(req, User.class);
        //验证实例是否有效
        Set<String> validator = BeanValidatorUtil.validator(User.class, user);

        if (validator.isEmpty()){
            //错误为空则成功 进行返回
            String s = UUID.randomUUID().toString();
            s = s.substring(1, 20);
            user.setUserId(s);
            UserServiceImpl userService = new UserServiceImpl();
            int insert = userService.insert(user);
            ResultAjax success = ResultAjax.success();
            if (insert <= 0){
                success.put(ResultAjax.DATA,"插入失败");
            }
            render(resp,success);

        }else {
            //返回错误信息
            String message = validator.iterator().next();
            super.render(resp,ResultAjax.error(message));
        }
    }
}
