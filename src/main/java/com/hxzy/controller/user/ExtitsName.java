package com.hxzy.controller.user;

import com.hxzy.common.BaseServlet;
import com.hxzy.common.ResultAjax;
import com.hxzy.enetity.User;
import com.hxzy.service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;

/**
 * @Description:
 * @Author: Hao
 * @Date: 2022/3/9 14:41
 */
@WebServlet(urlPatterns = "/api/titsname")
public class ExtitsName extends BaseServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            User user = getInstanceByParameters(req, User.class);
            UserServiceImpl userService = new UserServiceImpl();
            int i = userService.extitsName(user);
            ResultAjax success = ResultAjax.success();
            if (i > 0){
                success.put(ResultAjax.DATA,"已被使用");
            }
            render(resp,success);
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
