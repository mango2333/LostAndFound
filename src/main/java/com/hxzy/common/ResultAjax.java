package com.hxzy.common;

import com.github.pagehelper.PageInfo;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description:
 * @Author: TanBo
 * @Date: 2022/2/26 16:24
 */
public class ResultAjax extends HashMap<String, Object> {
    public final static String CODE = "code";
    public final static String MESSAGE = "message";
    public final static String DATA = "data";

    public static ResultAjax success(String message, Object data) {
        ResultAjax ajax = new ResultAjax();
        ajax.put(CODE, 200);
        ajax.put(MESSAGE, message);
        ajax.put(DATA, data);
        return ajax;
    }

    public static ResultAjax success() {
        return ResultAjax.success("ok", null);
    }

    public static ResultAjax success(String message) {
        return ResultAjax.success(message, null);
    }

    public static ResultAjax success(Object data) {
        return ResultAjax.success("ok", data);
    }

    public static ResultAjax success(PageInfo<?> pageInfo) {
        Map<String,Object> map = new HashMap<>();
        map.put("rows",pageInfo.getList());
        map.put("total",pageInfo.getTotal());
        return ResultAjax.success("ok", map);
    }

    public static ResultAjax error() {
        return ResultAjax.error(400, "error");
    }

    public static ResultAjax error(int code) {
        return ResultAjax.error(code, "error");
    }

    public static ResultAjax error(String message) {
        return ResultAjax.error(400, message);
    }

    public static ResultAjax error(int code, String message) {
        ResultAjax ajax = new ResultAjax();
        ajax.put(CODE, code);
        ajax.put(MESSAGE, message);
        ajax.put(DATA, null);
        return ajax;
    }
}
