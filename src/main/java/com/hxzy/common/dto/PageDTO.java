package com.hxzy.common.dto;

/**
 * @Description:
 * @Author: TanBo
 * @Date: 2022/2/25 11:32
 */
public class PageDTO {
    private Integer page;
    private Integer size;

    public PageDTO() {
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

}
