package com.hxzy.common.dto;

import java.util.HashMap;

/**
 * @Description:
 * @Author: TanBo
 * @Date: 2022/2/28 14:04
 */
public class BaseDTO {
    private HashMap<String,Object> params;

    public HashMap<String, Object> getParams() {
        return params;
    }

    public void setParams(HashMap<String, Object> params) {
        this.params = params;
    }
}
