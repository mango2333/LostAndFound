package com.hxzy.common;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.hxzy.common.dto.PageDTO;
import com.hxzy.util.StringUtil;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * @Description:
 * @Author: TanBo
 * @Date: 2022/2/26 16:33
 */
public class BaseServlet extends HttpServlet {


    protected <T> T getInstanceByRequest(HttpServletRequest req, Class<T> tClass) throws IOException, ParseException, InstantiationException, IllegalAccessException {
        req.setCharacterEncoding("UTF-8");
        String contentType = req.getContentType();
        if (null != contentType && contentType.toLowerCase().contains("json")){
            return getInstanceByJsonParameters(req, tClass);
        }else {
            return getInstanceByParameters(req, tClass);
        }
    }

    protected <T> T getInstanceByParameters(HttpServletRequest req, Class<T> tClass) throws InstantiationException, IllegalAccessException, ParseException {
        T instance = tClass.newInstance();
        Field[] declaredFields = tClass.getDeclaredFields();
        for (Field field : declaredFields) {
            String name = field.getName();
            Class type = field.getType();
            Object value = getParametersValue(req, name);
            if (null != value) {
                field.setAccessible(true);
                if (type == Integer.class && StringUtil.isInt(value.toString())) {
                    field.set(instance, new Integer(value.toString()));
                } else if (type == int.class && StringUtil.isInt(value.toString())) {
                    field.setInt(instance, Integer.parseInt(value.toString()));
                } else if (type == Double.class && StringUtil.isDigit(value.toString())) {
                    field.set(instance, new Double(value.toString()));
                } else if (type == Float.class && StringUtil.isDigit(value.toString())) {
                    field.set(instance, new Float(value.toString()));
                } else if (type == String.class) {
                    field.set(instance, value);
                } else if (type == Timestamp.class) {
                    field.set(instance, new Timestamp(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(value.toString()).getTime()));
                } else if (type == BigDecimal.class) {
                    field.set(instance,new BigDecimal(value.toString()));
                }
            }

        }
        return instance;
    }

    protected <T> T getInstanceByJsonParameters(HttpServletRequest req, Class<T> clazz) throws IOException {

        ServletInputStream inputStream = req.getInputStream();
        T instance = null;
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream,"UTF-8"));
        StringBuffer buffer = new StringBuffer();
        String str = "";
        while ((str = reader.readLine()) != null) {
            buffer.append(str);
        }
        instance = JSONObject.parseObject(buffer.toString(), clazz);
        return instance;
    }

    protected <T> List<T> getArrayByJsonParameters(HttpServletRequest req, Class<T> clazz) throws IOException {

        ServletInputStream inputStream = req.getInputStream();
        List<T> list;
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream,"UTF-8"));
        StringBuffer buffer = new StringBuffer();
        String str = "";
        while ((str = reader.readLine()) != null) {
            buffer.append(str);
        }
        list = JSONObject.parseArray(buffer.toString(), clazz);
        return list;
    }

    protected Object getParametersValue(HttpServletRequest req, String name) {
        String[] values = req.getParameterValues(name);
        if (null != values && values.length == 1) {
            return values[0];
        }
        return values;
    }

    /**
     * 开启分页
     *
     * @param req
     */
    protected void startPage(HttpServletRequest req) throws IOException {
        PageDTO pageDTO = getInstanceByJsonParameters(req, PageDTO.class);
        if (null != pageDTO && null != pageDTO.getPage() && null != pageDTO.getSize()) {
            PageHelper.startPage(pageDTO.getPage(), pageDTO.getSize());
        }

    }


    /**
     * 向客户端输出字符串
     *
     * @param text
     */
    protected void render(HttpServletResponse resp, String text) throws IOException {
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html; charset=utf-8");

        PrintWriter out = resp.getWriter();
        out.write(text);
        out.flush();
        out.close();
    }

    protected void render(HttpServletResponse resp, int count) throws IOException {
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("application/json;charset=UTF-8");

        ResultAjax json;
        if (count > 0) {
            json = ResultAjax.success();
        } else {
            json = ResultAjax.error();
        }
        String text = JSONObject.toJSONString(json);

        PrintWriter out = resp.getWriter();
        out.write(text);
        out.flush();
        out.close();
    }

    protected void render(HttpServletResponse resp, ResultAjax json) throws IOException {

        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("application/json;charset=UTF-8");

        String text = JSONObject.toJSONString(json);

        PrintWriter out = resp.getWriter();

        out.write(text);
        out.flush();
        out.close();
    }

}
